import { Component, OnInit } from '@angular/core';

import {CommonService} from '../services/common.service';

@Component({
  selector: 'amibaba-topnav',
  templateUrl: './topnav.component.html',
  styleUrls: ['./topnav.component.css']
})
export class TopnavComponent implements OnInit {
  title:  string;
  logo: string;
  countries: any;
  navLinks: Array<any>
  constructor(private cm:CommonService) { 
    this.title="amibaba";
    this.navLinks=[{icon:'', displayName: "Home"},
  {icon:'',displayName: "Products"},
  {icon:'',displayName: "Cart"},
  {icon:'',displayName: "Login"},
  {icon:'',displayName: "Register"},
  {icon:'',displayName: "Employees"},
  {icon:'',displayName: "Addproduct"},
  {icon:'',displayName: "Countries"}]

  this.countries=this.cm.getCountryList();
  }

  ngOnInit() {
  }

}
