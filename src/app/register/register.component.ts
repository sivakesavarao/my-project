import { Component, OnInit } from '@angular/core';
import { Register } from './register';
import { CommonService } from '../services/common.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  register: Register;
  countries: Array<any>
  constructor(private commonSvc:CommonService) {
    this.register = new Register();
    this.countries = this.commonSvc.getCountryList();
    this.commonSvc.getCountriesFromApi()
    .subscribe(y=>{
      console.log(y);
    })
  }

  ngOnInit() {
  }
  registerHandler() {
    console.log(this.register);
  }
}
