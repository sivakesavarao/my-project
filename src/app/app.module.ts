import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { RouterModule } from '@angular/router';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './register/register.component';
import { ProductsComponent } from './products/products.component';
import { TopnavComponent } from './topnav/topnav.component';
import { CartComponent } from './cart/cart.component';
import { FooterComponent } from './footer/footer.component';
import { EmployeesComponent } from './employees/employees.component';
import { AddproductComponent } from './addproduct/addproduct.component';
import { CommonService } from './services/common.service';
import { CountriesComponent } from './countries/countries.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    RegisterComponent,
    ProductsComponent,
    TopnavComponent,
    CartComponent,
    FooterComponent,
    EmployeesComponent,
    AddproductComponent,
    CountriesComponent,
    
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    RouterModule.forRoot([
      {path:"Cart", component:CartComponent},
      {path:"Products", component:ProductsComponent},
      {path:"Home", component:HomeComponent},
      {path:"Login", component:LoginComponent},
      {path:"Register", component:RegisterComponent},
      {path:"Employees", component:EmployeesComponent},
      {path:"Addproduct", component:AddproductComponent},
      {path:"Countries", component:CountriesComponent}
    ])
    
  ],
  providers: [CommonService],
  bootstrap: [AppComponent]
})
export class AppModule { }
