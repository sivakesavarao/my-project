import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

 import { Observable } from 'rxjs';
 import 'rxjs/Rx';

@Injectable()
export class CommonService {
    private countries: Array<any>
    private countriesUrl: string;
    constructor(private httpSvc: Http) {
        this.countries = [];
        this.countriesUrl = "assets/countries.json";
    }

    getCountryList(): Array<any> {
        this.countries = [{ text: "India", value: "IN" },
        { text: "Australia", value: "AU" },
        { text: "United States", value: "US" }];

        return this.countries;
    }

    getCountriesFromApi() {
       return  this.httpSvc.get(this.countriesUrl)
             .map(x => {
                
                let countryInfo = x.json();
                console.log(countryInfo);
                return countryInfo;
            });
    }
}
