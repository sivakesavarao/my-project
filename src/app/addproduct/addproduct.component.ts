import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-addproduct',
  templateUrl: './addproduct.component.html',
  styleUrls: ['./addproduct.component.css']
})
export class AddproductComponent implements OnInit {
  title:  string;
  proLinks  : Array<any>
  constructor() {
    this.title  = "Products";
    this.proLinks = [{proName:"iPhone",proPrice:"30000",proDesc:"iPhone",proCat:"Electronics"},
    {proName:"Samsung",proPrice:"60000",proDesc:"Samsung",proCat:"Electronics"},
    {proName:"Celkon",proPrice:"50000",proDesc:"Celkon",proCat:"Electronics"},
    {proName:"Apple",proPrice:"40000",proDesc:"Apple",proCat:"Electronics"}]
   }

  ngOnInit() {
  }

}
