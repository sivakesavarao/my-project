import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {

  title : string;
  empLinks  : Array<any>
  constructor() { 
    this.title  = "Table Data";
    this.empLinks = [{empName : "emp1", mail  : "emp1@gmail.com", sal : "200000"},
    {empName : "emp2", mail  : "emp2@gmail.com", sal : "300000"},
    {empName : "emp3", mail  : "emp3@gmail.com", sal : "400000"}]

  }

  ngOnInit() {
  }

}
